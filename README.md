# public-key-retriever

Wrapper for PublickeyJS library ( https://github.com/diafygi/publickeyjs)


# For execute the sample application

1) Clone the application
2) Execute following commands
    *  cd public-key-retriever
    *  npm install
    *  cd sample
    *  node app.js
    