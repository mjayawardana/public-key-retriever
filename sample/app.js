const pgpKeyWrapper = require('./../PGPKeyWrapper');

//For setting up key servers if any. unless it will setup default servers
pgpKeyWrapper.setKeyServers(["https://keybase.io/"]);


//For search by Name and max result size
pgpKeyWrapper.search("verheesen",5, function(results){
    getKeyServerResultsList(results);
});

//Getting the public key by key id
function getKeyServerResultsList(results){
    console.log(results);
    var keyId = results[0].keyid;
    console.log("key Id : " + keyId);
    pgpKeyWrapper.getPublicKeyByKeyId(keyId);
}

//Getting the public key directly by using name
pgpKeyWrapper.getPublicKeyByName("verheesen", function(publicKey){
    console.log(publicKey);
});