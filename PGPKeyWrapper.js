const publicKey = require('./publickey');

var pk = new publicKey.PublicKey();

// Set Key servers manually. if you don't call this method default keyservers will be used.
//  Param : 
//      keyserver - key server array. need to be a array object
function setKeyServers(keyserver){
    pk = new publicKey.PublicKey(keyserver);
}

// Search for public key ids on the initialized keyservers.
//  Param : 
//      query - String to search for (usually an email, name, or username).
//      maxResult - No of Max results needed
//      callback - callback function to retrieve the data
function search(query ,maxResult,callback){
    pk.search(query, function(results, errorCode){
       if( errorCode !== null) {
             console.log(errorCode);
             return null;
        } else {
            return callback(getResults(results,maxResult));
        } 
    });
}

// Get Public key from key id.
//  Param : 
//      keyId - public key id.
function getPublicKeyByKeyId(keyId){
    pk.get(keyId, function(publicKey, errorCode){
        errorCode !== null ? console.log(errorCode) : console.log(publicKey);
    });
}

//  Get Public key from user name. Only first public key will be returned
//  Param : 
//      query - String to search for (usually an email, name, or username).
//      callback - callback function to retrieve the data
function getPublicKeyByName(query,callback){
    pk.search(query, function(results, errorCode){
        if( errorCode !== null) {
              console.log(errorCode);
              return null;
         } else {
            var result = getResults(results,1);
            pk.get(result[0].keyid, function(publicKey, errorCode){
                if( errorCode !== null) {
                    console.log(errorCode);
                    return null;
               } else {
                   return callback(publicKey);
               }
            });
         } 
     });    
}

function getResults(resultList, maxResult){
    var results = []; ;
    maxResult = maxResult || resultList.length;
    resultList.forEach(function(value){
        --maxResult ;
        if(maxResult>=0)
            results.push(value);
    });
    return results;
}

var Exported = {
    setKeyServers:setKeyServers,
    search: search,
    getPublicKeyByKeyId: getPublicKeyByKeyId,
    getPublicKeyByName: getPublicKeyByName
 }
 
 module.exports = Exported;

